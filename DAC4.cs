﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UEI.QuickSet;
using System.Data;

//using System.Threading.Tasks;

namespace UEI.QuickSet.DAC4Lib
{
   
        public class DAC4
        {
            FileHeader _fileHeader;
            FileToc _fileToc;
            BrandRecordHeader _dac3aBrandRecordHeader;
            BrandRecordHeader _dac3aKeylabelHeader;
            BrandCollections _dac3aBrandRecords;
            ARDRecordHeader _dac3aArdRecordHeader;
            ARDRecordHeader _dac3aArdRecordHeaderOnly;
            ARDRecordTOCHeader _dac3aArdRecordTocHeader;
            ARDRecordTOCHeader _dac3aArdRecordTocHeaderOnly;
            List<ARDRecordTOCEntryExclusive> _dac3aArdDataRecordTOCEntryCollections;
            List<ARDRecordTOCEntry> _dac3aArdDataRecordTOCEntry;
            ARDEntryRecord _dac3aEntryRecord;

            FileEndRecord _dac3aFER;

            List<FPAddress> _fpaddress;
            int _stAddress, _edAddress = 0;

            public void SetFileHeader(string filetype, string fileversion, string fileflags)
            {
                _fileHeader = new FileHeader();
                _fileHeader.FileType = filetype;
                _fileHeader.FileVersion = fileversion;
                _fileHeader.FileFlags = fileflags;

                _fileHeader.ConvertToByte();
                _fileHeader.ConvertToLittleEndian();
            }
            public void SetTocHeader(string type, int offsetaddress)
            {
                _fileToc = new FileToc();
                _fileToc.Tocheader.Name = type;
                _fileToc.Tocheader.Offset = 18;
                _fileToc.TocCollection = new List<FileToc.TocData>();
                _fileToc.Tocheader.ConvertToByte();
                _fileToc.Tocheader.ConvertToLittleEndian();

            }

            //public void SetTocHeader_v3(string type, int offsetaddress)
            //{
            //    _fileToc = new FileToc();
            //    _fileToc.Tocheader.Name = type;
            //    _fileToc.Tocheader.Offset = 24;

            //    _fileToc.Tocheader.ConvertToByte();
            //    _fileToc.Tocheader.ConvertToLittleEndian();

            //}
            public void SetBrandRecord(List<string> brands)
            {
                _dac3aBrandRecords = new BrandCollections();
                _dac3aBrandRecords.Brands = new List<BrandRecord>();

                BrandRecord _brnd = new BrandRecord();
                foreach (string brand in brands)
                {

                    _brnd = new BrandRecord();
                    _brnd.BrandName = brand;
                    _brnd.ConvertToByte();
                    _brnd.ConvertToEndian();
                    _brnd.RecordSize = _brnd.ByteBrandName.Length;
                    _brnd.ConvertToByte();
                    _brnd.ConvertToEndian();

                    _dac3aBrandRecords.Brands.Add(_brnd);
                }

            }
            public void SetBrandRecordHeader(string header)
            {
                _dac3aBrandRecordHeader = new BrandRecordHeader();

                _dac3aBrandRecordHeader.BrandHeader = header;
                _dac3aBrandRecordHeader.BrandRecordSize = GetBrandRecordSize();
                _dac3aBrandRecordHeader.BrandsCount = _dac3aBrandRecords.Brands.Count;

                _dac3aBrandRecordHeader.ConvertToByte();
                _dac3aBrandRecordHeader.ConvertToEndian();

            }

            public void SetKeyLabelRecordHeader(string header,int size,int reccount)
            {
                _dac3aKeylabelHeader = new BrandRecordHeader();

                _dac3aKeylabelHeader.BrandHeader = header;
                _dac3aKeylabelHeader.BrandRecordSize = size;
                _dac3aKeylabelHeader.BrandsCount = reccount;

                _dac3aKeylabelHeader.ConvertToByte();
                _dac3aKeylabelHeader.ConvertToEndian();

            }
            DataTable _dtbyteKeyLableRecords;
            public void SetKeyLabelRecords(DataTable _dt)
            {
                _dtbyteKeyLableRecords = new DataTable();
                _dtbyteKeyLableRecords = _dt;
            }

            private int GetBrandRecordSize()
            {
                int _size = 0;
                foreach (BrandRecord br in _dac3aBrandRecords.Brands)
                {
                    _size += br.ByteRecordSize.Length;
                    _size += br.ByteBrandName.Length;
                }
                return _size;
            }

            private string GetBrandIndex(string brand)
            {
                string _status = null;
                int _index = 0;
                foreach (BrandRecord _br in _dac3aBrandRecords.Brands)
                {
                    if (brand == _br.BrandName)
                    {
                        _status = _index.ToString();
                        break;
                    }
                    else
                        _index++;

                }
                return _status;
            }
            private string GetUEITypeID(string devicename)
            {
                string _typeId = string.Empty;
                switch (devicename)
                {
                    case "TV":
                    case "Television":
                        {
                            _typeId = "T";
                            break;
                        }
                    case "Audio":
                        {
                            _typeId = "A";
                            break;
                        }
                    case "CBL":
                    case "CABLE":
                    case "Cable":
                        {
                            _typeId = "C";
                            break;
                        }
                    case "STB":
                    case "satellite":
                    case "Satellite":
                    case "SAT":
                    case "sat":

                        {
                            _typeId = "S";
                            break;
                        }
                    case "AMP":
                        {
                            _typeId = "R";
                            break;
                        }
                    case "VA":
                    case "video accessories":

                        {
                            _typeId = "N";
                            break;
                        }
                    case "SB":
                    case "Sound Bar":
                    case "SoundBar":
                        {
                            _typeId = "M";
                            break;
                           
                        }
                }

                return _typeId;
            }
            private List<string> GetIdList(string idlist)
            {
                List<string> ids = new List<string>();
                string[] sep = { "," };
                string[] data = idlist.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                foreach (string id in data)
                {
                    ids.Add(id);
                }
                return ids;
            }
            private List<string> GetIdList(Dictionary<string, string> idlist)
            {
                List<string> _idlist = new List<string>();
                foreach (KeyValuePair<string, string> ids in idlist)
                {
                    _idlist.Add(ids.Key);
                }
                return _idlist;
            }
            private List<string> GetKeyIdList(Dictionary<string, string> idlist)
            {
                List<string> _keyidlist = new List<string>();
                foreach (KeyValuePair<string, string> ids in idlist)
                {
                    _keyidlist.Add(ids.Value);
                }
                return _keyidlist;
            }

            public void SetTocHeaderCollection(Dictionary<string, int> _tocCollections)
            {
                FileToc.TocData _tocdata = new FileToc.TocData();
                //_fileToc = new FileToc();
               
                foreach (KeyValuePair<string, int> _fToc in _tocCollections)
                {
                    _tocdata = new FileToc.TocData();
                    _tocdata.Name = _fToc.Key;
                    _tocdata.Offset = _fToc.Value;

                    _tocdata.ConvertToByte();
                    _tocdata.ConvertToLittleEndian();
                    _fileToc.TocCollection.Add(_tocdata);

                }
            }
            public void SetArdRecordHeader(string type, FunctionRuleMapData _frm)
            {
                _dac3aArdRecordHeaderOnly = new ARDRecordHeader();
                _dac3aArdRecordHeaderOnly.Type = type;
                _dac3aArdRecordHeaderOnly.SizeOfARDRecords = 0;
                _dac3aArdRecordHeaderOnly.Ard = new List<ARDRecordTOCHeader>();
                _dac3aArdRecordHeaderOnly.ConvertToByte();
                _dac3aArdRecordHeaderOnly.ConvertToLittleEndian();
            }
            public void SetArdRecordHeader(string type, string tocheadertype, FunctionRuleMapData _frm)
            {
                _dac3aArdRecordHeader = new ARDRecordHeader();
                _dac3aArdRecordHeader.Type = type;
                _dac3aArdRecordHeader.SizeOfARDRecords = 0;
                _dac3aArdRecordHeader.Ard = new List<ARDRecordTOCHeader>();
                SetFingerPrintCollections(_frm);
                ARDRecordTOCHeader _ardTocHeader = new ARDRecordTOCHeader();
                _ardTocHeader = SetArdRecordTocHeader(tocheadertype, _frm);

                _dac3aArdRecordHeader.Ard.Add(_ardTocHeader);

                _dac3aArdRecordHeader.ConvertToByte();
                _dac3aArdRecordHeader.ConvertToLittleEndian();
            }

            public void SetArdRecordTocHeaderOnly(string type, FunctionRuleMapData _frm)
            {
                _dac3aArdRecordTocHeaderOnly = new ARDRecordTOCHeader();
                _dac3aArdRecordTocHeaderOnly.Type = type;
                _dac3aArdRecordTocHeaderOnly.TocRecordSize = 0;
                _dac3aArdRecordTocHeaderOnly.ArdDataCount = _frm.FigerPrintRecords.Count;
                _dac3aArdRecordTocHeaderOnly.ArdRecordTocEntryCollection = new List<ARDRecordTOCEntry>();
                _dac3aArdRecordTocHeaderOnly.ConvertToByte();
                _dac3aArdRecordTocHeaderOnly.ConvertToLittleEndian();
            }
            public ARDRecordTOCHeader SetArdRecordTocHeader(string type, FunctionRuleMapData _frm)
            {
                _dac3aArdRecordTocHeader = new ARDRecordTOCHeader();
                _dac3aArdRecordTocHeader.Type = type;
                _dac3aArdRecordTocHeader.TocRecordSize = 0;
                _dac3aArdRecordTocHeader.ArdDataCount = _frm.FigerPrintRecords.Count;
                _dac3aArdRecordTocHeader.ArdRecordTocEntryCollection = new List<ARDRecordTOCEntry>();
                _dac3aArdRecordTocHeader.ArdRecordTocEntryCollection = SetArdRecordTocEntryCollections(_frm);
                _dac3aArdRecordTocHeader.ConvertToByte();
                _dac3aArdRecordTocHeader.ConvertToLittleEndian();
                return _dac3aArdRecordTocHeader;
            }

            public void SetFingerPrintCollections(FunctionRuleMapData _frm)
            {
                _dac3aArdDataRecordTOCEntryCollections = new List<ARDRecordTOCEntryExclusive>();
                ARDRecordTOCEntryExclusive _ardTocExclsive = new ARDRecordTOCEntryExclusive();
                for (int i = 0; i < _frm.FigerPrintRecords.Count; i++)
                {
                    _ardTocExclsive = new ARDRecordTOCEntryExclusive();
                    string _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Replace("0x", "").Trim();
                    long decValue = Convert.ToInt64(_temp, 16);
                    _ardTocExclsive.Key = decValue;
                    _ardTocExclsive.Offset = 0;
                    _ardTocExclsive.ConvertToByte();
                    _ardTocExclsive.ConvertToLittleEndian();
                    _dac3aArdDataRecordTOCEntryCollections.Add(_ardTocExclsive);
                }
            }

            public List<ARDRecordTOCEntry> SetArdRecordTocEntryCollections(FunctionRuleMapData _frm)
            {
                List<ARDRecordTOCEntry> _ardtocentrycol = new List<ARDRecordTOCEntry>();
                ARDRecordTOCEntry _basedata = new ARDRecordTOCEntry();

                for (int i = 0; i < _frm.FigerPrintRecords.Count; i++)
                {
                    string _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Replace("0x", "").Trim();
                    long decValue = Convert.ToInt64(_temp, 16);
                    _basedata = new ARDRecordTOCEntry();
                    _basedata.Key = decValue;
                    _basedata.Offset = 0;

                    _basedata.ArdentryRecords = new List<ARDEntryRecord>();
                    _basedata.ArdentryRecords = SetARDEntryRecord(_frm, _frm.FigerPrintRecords[i].FingerprintRecord);

                    _basedata.ConvertToByte();
                    _basedata.ConvertToLittleEndian();
                    _ardtocentrycol.Add(_basedata);
                }

                return _ardtocentrycol;

            }
            public void SetARDEntryRecords(FunctionRuleMapData _frm)
            {
                _dac3aArdDataRecordTOCEntry = new List<ARDRecordTOCEntry>();

                ARDRecordTOCEntry _basedata = new ARDRecordTOCEntry();
                _fpaddress = new List<FPAddress>();
                for (int i = 0; i < _frm.FigerPrintRecords.Count; i++)
                {
                    _basedata = new ARDRecordTOCEntry();
                    //_fp = new FPAddress();
                    string _temp = string.Empty;
                    if (_frm.FigerPrintRecords[i].FingerprintRecord.Contains("0x") || _frm.FigerPrintRecords[i].FingerprintRecord.Contains("0X"))
                    {
                        if (_frm.FigerPrintRecords[i].FingerprintRecord.Contains("0x"))
                        {
                            _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Replace("0x", "").Trim();
                        }
                        else
                        {
                            _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Replace("0X", "").Trim();
                        }
                    }
                    else
                    {
                        _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Trim();
                    }
                    //string _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Replace("0x", "").Trim();
                    long decValue = Convert.ToInt64(_temp, 16);
                    _basedata = new ARDRecordTOCEntry();
                    _basedata.Key = decValue;
                    //_fp.fingerprint = decValue;
                    _basedata.Offset = 0;

                    _basedata.ArdentryRecords = new List<ARDEntryRecord>();
                    _basedata.ArdentryRecords = SetARDEntryRecord(_frm, _frm.FigerPrintRecords[i].FingerprintRecord);

                    _basedata.ConvertToByte();
                    _basedata.ConvertToLittleEndian();
                    //_fp.endaddress = _basedata.GetEndAddress(0);
                    _dac3aArdDataRecordTOCEntry.Add(_basedata);
                    //_fpaddress.Add(_fp);
                }
            }
            public List<ARDEntryRecord> SetARDEntryRecord(FunctionRuleMapData _frm, string fingerprint)
            {
                List<ARDEntryRecord> ardentryrecordlst = new List<ARDEntryRecord>();
                ARDEntryRecord ardentryrecord = new ARDEntryRecord();
                FPAddress _fpa = new FPAddress();
                string _prevDevice = string.Empty;
                for (int i = 0; i < _frm.FigerPrintRecords.Count; i++)
                {
                    if (fingerprint == _frm.FigerPrintRecords[i].FingerprintRecord)
                    {
                        string _temp = _frm.FigerPrintRecords[i].FingerprintRecord.Replace("0x", "").Trim();
                        long decValue = Convert.ToInt64(_temp, 16);
                        _fpa.fingerprint = decValue;
                        _fpa.devices = new List<FPAddress.dev>();
                        FPAddress.dev _devi = new FPAddress.dev();
                        for (int dev = 0; dev < _frm.FigerPrintRecords[i].Devices.Count; dev++)
                        {
                            ardentryrecord = new ARDEntryRecord();
                            _devi = new FPAddress.dev();
                            ardentryrecord.ArdRecordHeader = new List<ARDEntryRecord.ARDEntryRecordHeader>();
                            ARDEntryRecord.ARDEntryRecordHeader _header = new ARDEntryRecord.ARDEntryRecordHeader();

                            _header.UeiTypeId = GetUEITypeID(_frm.FigerPrintRecords[i].Devices[dev].Name);
                            _devi.devname = _header.UeiTypeId;
                            _header.EntrySize = 0;
                            _header.BrandRecords = new List<ARDEntryRecord.BrandRecords>();
                            for (int j = 0; j < _frm.FigerPrintRecords[i].Devices[dev].Brands.Count; j++)
                            {
                                ARDEntryRecord.BrandRecords _br = new ARDEntryRecord.BrandRecords();
                                _br.Name = _frm.FigerPrintRecords[i].Devices[dev].Brands[j].Name;
                                _br.BrandIdIndex = Int32.Parse(GetBrandIndex(_frm.FigerPrintRecords[i].Devices[dev].Brands[j].Name));
                                _br.KeyRuleCount = _frm.FigerPrintRecords[i].Devices[dev].Brands[j].Keys.Count;
                                _br.FunctionRuleMaps = new List<FunctionRuleMap>();
                                _br.FunctionRuleMaps = SetFunctionRuleMap(_frm, _frm.FigerPrintRecords[i].FingerprintRecord, _frm.FigerPrintRecords[i].Devices[dev].Name, _frm.FigerPrintRecords[i].Devices[dev].Brands[j].Name);
                                _header.ConvertToByte();
                                _header.ConvertToLittleEndian();

                                _header.BrandRecords.Add(_br);
                            }
                            _devi.endaddress = fnsize;
                            fnsize = 0;
                            ardentryrecord.ArdRecordHeader.Add(_header);
                            ardentryrecord.ConvertToByte();
                            ardentryrecord.ConvertToLittleEndian();
                            ardentryrecordlst.Add(ardentryrecord);
                            _fpa.devices.Add(_devi);
                        }
                        _fpaddress.Add(_fpa);

                    }
                }

                return ardentryrecordlst;
            }
            int fnsize = 0;
            public List<FunctionRuleMap> SetFunctionRuleMap(FunctionRuleMapData _frm, string fingerprint, string device, string brand)
            {
                List<FunctionRuleMap> _fnRuleMap = new List<FunctionRuleMap>();
                FunctionRuleMap _fnrule = new FunctionRuleMap();
                fnsize = 0;
                for (int i = 0; i < _frm.FigerPrintRecords.Count; i++)
                {
                    if (fingerprint == _frm.FigerPrintRecords[i].FingerprintRecord)
                    {
                        for (int j = 0; j < _frm.FigerPrintRecords[i].Devices.Count; j++)
                        {
                            if (device == _frm.FigerPrintRecords[i].Devices[j].Name)
                            {
                                for (int k = 0; k < _frm.FigerPrintRecords[i].Devices[j].Brands.Count; k++)
                                {
                                    if (brand == _frm.FigerPrintRecords[i].Devices[j].Brands[k].Name)
                                    {
                                        for (int rules = 0; rules < _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys.Count; rules++)
                                        {
                                            _fnrule = new FunctionRuleMap();
                                            _fnrule.UeiFunctionId = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Id;
                                            _fnrule.CecDataSize = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Cecinfo.DataSize;

                                            _fnrule.CecPriority = Convert.ToInt32(_frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Cecinfo.Priority.Replace("0x", "").Trim(), 16);
                                            _fnrule.CecCommandCount = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Cecinfo.Commandcount;
                                            //Need to add CEC commands here
                                            _fnrule.CecCommands = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Cecinfo.Commands;
                                            _fnrule.IrDataSize = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Irinfo.DataSize;
                                            _fnrule.IrPriority = int.Parse(_frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Irinfo.Priority.Replace("0x", "").Trim());

                                            if (_frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Irinfo.IrCodesetcount > 0)
                                            {
                                                _fnrule.IrCodesetCount = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Irinfo.IrCodesetcount;
                                                _fnrule.IrCodesets = GetIdList(_frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Irinfo.IrCodesetKeyId);

                                                _fnrule.IrKeyData = GetKeyIdList(_frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Irinfo.IrCodesetKeyId);
                                            }
                                            _fnrule.IpDataSize = _frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].Ipinfo.DataSize;
                                            //_fnrule.UeiFunctionId=_frm.FigerPrintRecords[i].Devices[j].Brands[k].Keys[rules].
                                            _fnrule.ConvertToByte();
                                            _fnrule.ConvertToLittleEndian();
                                            fnsize += _fnrule.GetEndAddress(0);
                                            _fnRuleMap.Add(_fnrule);
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                return _fnRuleMap;

            }
            public void SetArdRecords(FunctionRuleMapData _frm)
            {

            }

            public void SetFileEndRecord(string type, int checksum)
            {
                _dac3aFER = new FileEndRecord();
                _dac3aFER.Type = type;
                _dac3aFER.Checksum = checksum;
                _dac3aFER.ConvertToByte();
                _dac3aFER.ConvertToLittleEndian();
            }

            private void UpdateFileTocOffsets()
            {
                
                foreach (KeyValuePair<string, long> addr in _fileTocAddress)
                {
                    switch (addr.Key)
                    {
                        case "TC":
                            {
                                for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                                {
                                    if (_fileToc.TocCollection[i].Name == "TC")
                                    {
                                        _fileToc.TocCollection[i].Offset = 18;
                                        _fileToc.TocCollection[i].ConvertToByte();
                                        _fileToc.TocCollection[i].ConvertToLittleEndian();
                                    }
                                }
                                break;
                            }
                        case "BH":
                            {
                                for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                                {
                                    if (_fileToc.TocCollection[i].Name == "BH")
                                    {
                                        _fileToc.TocCollection[i].Offset = addr.Value;
                                        _fileToc.TocCollection[i].ConvertToByte();
                                        _fileToc.TocCollection[i].ConvertToLittleEndian();
                                    }
                                }
                                break;
                            }
                        case "AR":
                            {
                                for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                                {
                                    if (_fileToc.TocCollection[i].Name == "AR")
                                    {
                                        _fileToc.TocCollection[i].Offset = addr.Value;
                                        _fileToc.TocCollection[i].ConvertToByte();
                                        _fileToc.TocCollection[i].ConvertToLittleEndian();
                                    }
                                }
                                break;
                            }
                        //case "IP":
                        //    {
                        //        for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                        //        {
                        //            if (_fileToc.TocCollection[i].Name == "IP")
                        //            {
                        //                _fileToc.TocCollection[i].Offset = addr.Value;
                        //                _fileToc.TocCollection[i].ConvertToByte();
                        //                _fileToc.TocCollection[i].ConvertToLittleEndian();
                        //            }
                        //        }
                        //        break;
                        //    }
                        //case "CE":
                        //    {
                        //        for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                        //        {
                        //            if (_fileToc.TocCollection[i].Name == "CE")
                        //            {
                        //                _fileToc.TocCollection[i].Offset = addr.Value;
                        //                _fileToc.TocCollection[i].ConvertToByte();
                        //                _fileToc.TocCollection[i].ConvertToLittleEndian();
                        //            }
                        //        }
                        //        break;
                        //    }
                        //case "IF":
                        //    {
                        //        for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                        //        {
                        //            if (_fileToc.TocCollection[i].Name == "IF")
                        //            {
                        //                _fileToc.TocCollection[i].Offset = addr.Value;
                        //                _fileToc.TocCollection[i].ConvertToByte();
                        //                _fileToc.TocCollection[i].ConvertToLittleEndian();
                        //            }
                        //        }
                        //        break;
                        //    }
                        case "FE":
                            {
                                for (int i = 0; i < _fileToc.TocCollection.Count; i++)
                                {
                                    if (_fileToc.TocCollection[i].Name == "FE")
                                    {
                                        _fileToc.TocCollection[i].Offset = addr.Value;
                                        _fileToc.TocCollection[i].ConvertToByte();
                                        _fileToc.TocCollection[i].ConvertToLittleEndian();
                                    }
                                }
                                break;
                            }
                    }
                }
            }
            private void UpdateBrandRecordSize(int size)
            {
                _dac3aBrandRecordHeader.BrandRecordSize = size;
                _dac3aBrandRecordHeader.ConvertToByte();
                _dac3aBrandRecordHeader.ConvertToEndian();
            }
            private void UpdateARDDataRecordsSize(int size)
            {
                _dac3aArdRecordHeaderOnly.SizeOfARDRecords = size;
                _dac3aArdRecordHeaderOnly.ConvertToByte();
                _dac3aArdRecordHeaderOnly.ConvertToLittleEndian();
            }
            private void UpdateARDRecordTocHeaderSize(int size)
            {
                _dac3aArdRecordTocHeaderOnly.TocRecordSize = size;
                _dac3aArdRecordTocHeaderOnly.ConvertToByte();
                _dac3aArdRecordTocHeaderOnly.ConvertToLittleEndian();
            }
            private void UpdateFingerPrintOffsets(Dictionary<long, int> fpaddress)
            {

                foreach (KeyValuePair<long, int> addr in fpaddress)
                {
                    foreach (ARDRecordTOCEntryExclusive _ard in _dac3aArdDataRecordTOCEntryCollections)
                    {
                        if (addr.Key == _ard.Key)
                        {
                            _ard.Offset = addr.Value;
                            _ard.ConvertToByte();
                            _ard.ConvertToLittleEndian();
                            break;
                        }
                    }
                }

            }
            private void UpdateARDEntryRecordSize()
            {
                foreach (FPAddress _fpaddr in _fpaddress)
                {

                    for (int i = 0; i < _dac3aArdDataRecordTOCEntry.Count; i++)
                    {
                        if (_fpaddr.fingerprint == _dac3aArdDataRecordTOCEntry[i].Key)
                        {
                            foreach (FPAddress.dev Dev in _fpaddr.devices)
                            {
                                for (int j = 0; j < _dac3aArdDataRecordTOCEntry[i].ArdentryRecords.Count; j++)
                                {
                                    for (int k = 0; k < _dac3aArdDataRecordTOCEntry[i].ArdentryRecords[j].ArdRecordHeader.Count; k++)
                                    {
                                        if (Dev.devname == _dac3aArdDataRecordTOCEntry[i].ArdentryRecords[j].ArdRecordHeader[k].UeiTypeId)
                                        {
                                            _dac3aArdDataRecordTOCEntry[i].ArdentryRecords[j].ArdRecordHeader[k].EntrySize = Dev.endaddress;
                                            _dac3aArdDataRecordTOCEntry[i].ArdentryRecords[j].ArdRecordHeader[k].ConvertToByte();
                                            _dac3aArdDataRecordTOCEntry[i].ArdentryRecords[j].ArdRecordHeader[k].ConvertToLittleEndian();
                                            break;
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
            }
            private void UpdateCheckSum(Int64 checksum)
            {
                _dac3aFER.Checksum = checksum;
                _dac3aFER.ConvertToByte();
                _dac3aFER.ConvertToLittleEndian();
            }

            private Int64 CalculateChecksum(List<byte> bytedata)
            {
                Int64 _checksum = 0;
                foreach (byte by in bytedata)
                {
                    _checksum ^= by;
                }
                return _checksum;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                List<byte> _tempData = new List<byte>();
                int _addressCounter = 0;
                _tempData = _fileHeader.GetByteData();

                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();

                _tempData = _fileToc.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aBrandRecordHeader.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aBrandRecords.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aArdRecordHeaderOnly.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aArdRecordTocHeaderOnly.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                foreach (ARDRecordTOCEntryExclusive _ardEx in _dac3aArdDataRecordTOCEntryCollections)
                {
                    List<byte> _te = _ardEx.GetByteData();
                    foreach (byte by in _te)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }
                }

                _tempData = new List<byte>();
                foreach (ARDRecordTOCEntry _ardtoc in _dac3aArdDataRecordTOCEntry)
                {
                    List<byte> _te = _ardtoc.GetByteData();
                    foreach (byte by in _te)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }
                }

                

                _tempData = new List<byte>();
                _tempData = _dac3aFER.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                return _byteData;
            }
            public List<byte> GetConvertedByteData()
            {
                List<byte> _byteData = new List<byte>();
                List<byte> _tempData = new List<byte>();
                int _addressCounter = 0;
                _tempData = _fileHeader.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = _fileToc.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aBrandRecordHeader.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aBrandRecords.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aArdRecordHeaderOnly.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aArdRecordTocHeaderOnly.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                foreach (ARDRecordTOCEntryExclusive _ardEx in _dac3aArdDataRecordTOCEntryCollections)
                {
                    List<byte> _te = _ardEx.GetConvertedData();
                    foreach (byte by in _te)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }
                }
                _tempData = new List<byte>();
                foreach (ARDRecordTOCEntry _ardtoc in _dac3aArdDataRecordTOCEntry)
                {
                    List<byte> _te = _ardtoc.GetConvertedData();
                    foreach (byte by in _te)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }
                }

                

                _tempData = new List<byte>();
                _tempData = _dac3aFER.GetConvertedData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }



                return _byteData;
            }

            int _sizeofbrandrecords = 0;

            Int64 _sizeOfARDDataRecordsstart = 0;
            Int64 _sizeOfARDDataRecordsend = 0;
            Int64 SizeOfARDDataRecords = 0;

            int _sizeofARDTOCHeaderstart = 0;
            int _sizeofARDTOCHeaderend = 0;
            int _sizeofARDTOCdata = 0;
            Dictionary<string, Int64> _fileTocAddress;
            Dictionary<long, int> _fingerprintaddress;
            public void CalculateAddress()
            {
                _fileTocAddress = new Dictionary<string, long>();
                _fingerprintaddress = new Dictionary<long, int>();
                int _fingerprintcounter = 0;

                List<byte> _byteData = new List<byte>();
                List<byte> _tempData = new List<byte>();
                int _addressCounter = 0;
                _tempData = _fileHeader.GetByteData();

                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                

                _tempData = _fileToc.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _fileTocAddress.Add("BH", _addressCounter);
                _tempData = _dac3aBrandRecordHeader.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _tempData = _dac3aBrandRecords.GetByteData();
                _sizeofbrandrecords = _dac3aBrandRecords.GetEndAddress(0);
                UpdateBrandRecordSize(_sizeofbrandrecords);
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                _fileTocAddress.Add("AR", _addressCounter);
                _tempData = _dac3aArdRecordHeaderOnly.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();

                _sizeOfARDDataRecordsstart = _addressCounter;
                _sizeofARDTOCHeaderstart = _addressCounter;
                _tempData = _dac3aArdRecordTocHeaderOnly.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }

                _tempData = new List<byte>();
                foreach (ARDRecordTOCEntryExclusive _ardEx in _dac3aArdDataRecordTOCEntryCollections)
                {

                    List<byte> _te = _ardEx.GetByteData();
                    foreach (byte by in _te)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }
                }

                _tempData = new List<byte>();
                _sizeofARDTOCHeaderend = _addressCounter;
                _sizeofARDTOCdata = _sizeofARDTOCHeaderend - _sizeofARDTOCHeaderstart;
                foreach (ARDRecordTOCEntry _ardtoc in _dac3aArdDataRecordTOCEntry)
                {
                    _fingerprintaddress.Add(_ardtoc.Key, _addressCounter);
                    _fingerprintcounter++;
                    List<byte> _te = _ardtoc.GetByteData();
                    foreach (byte by in _te)
                    {
                        _byteData.Add(by);
                        _addressCounter++;
                    }
                }
                _sizeOfARDDataRecordsend = _addressCounter;
                SizeOfARDDataRecords = _sizeOfARDDataRecordsend - _sizeOfARDDataRecordsstart;

                

                _tempData = new List<byte>();
                _fileTocAddress.Add("FE", _addressCounter);
                _tempData = _dac3aFER.GetByteData();
                foreach (byte by in _tempData)
                {
                    _byteData.Add(by);
                    _addressCounter++;
                }


                UpdateFileTocOffsets();
                UpdateARDDataRecordsSize((int)SizeOfARDDataRecords);
                UpdateARDRecordTocHeaderSize(_sizeofARDTOCdata);
                UpdateFingerPrintOffsets(_fingerprintaddress);
                UpdateARDEntryRecordSize();
                Int64 checksum = CalculateChecksum(_byteData);
                UpdateCheckSum(checksum);
            }
        }

        public class FileHeader
        {
            string _fileType;

            public string FileType
            {
                get { return _fileType; }
                set { _fileType = value; }
            }
            byte[] _byteFileType;

            public byte[] ByteFileType
            {
                get { return _byteFileType; }
                set { _byteFileType = value; }
            }
            byte[] _leFileType;

            public byte[] LeFileType
            {
                get { return _leFileType; }
                set { _leFileType = value; }
            }
            string _fileVersion;

            public string FileVersion
            {
                get { return _fileVersion; }
                set { _fileVersion = value; }
            }
            byte[] _byteFileVersion;

            public byte[] ByteFileVersion
            {
                get { return _byteFileVersion; }
                set { _byteFileVersion = value; }
            }
            byte[] _leFileVersion;

            public byte[] LeFileVersion
            {
                get { return _leFileVersion; }
                set { _leFileVersion = value; }
            }
            string _fileFlags;

            public string FileFlags
            {
                get { return _fileFlags; }
                set { _fileFlags = value; }
            }
            byte[] _byteFileFlags;

            public byte[] ByteFileFlags
            {
                get { return _byteFileFlags; }
                set { _byteFileFlags = value; }
            }
            byte[] _leFileFlags;

            public byte[] LeFileFlags
            {
                get { return _leFileFlags; }
                set { _leFileFlags = value; }
            }

            public void ConvertToByte()
            {

                ByteFileType = UEI.QuickSet.Conversion.ConvertToByte(FileType, ByteFileType.Length);
                ByteFileVersion = UEI.QuickSet.Conversion.ConvertToByte(Convert.ToInt32(FileVersion), ByteFileVersion.Length);
                ByteFileFlags = UEI.QuickSet.Conversion.ConvertToByte(Convert.ToInt32(FileFlags), ByteFileFlags.Length);


            }

            public void ConvertToLittleEndian()
            {


                LeFileType = ByteFileType;
                LeFileVersion = UEI.QuickSet.Conversion.ConvertToLittleEndian(ByteFileVersion, ByteFileVersion.Length);
                //LeFileVersion = ByteFileVersion;
                LeFileFlags = UEI.QuickSet.Conversion.ConvertToLittleEndian(ByteFileFlags, ByteFileFlags.Length);

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteFileType.Length + ByteFileVersion.Length + ByteFileFlags.Length;
                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _buffer = new List<byte>();

                foreach (byte _by in ByteFileType)
                {
                    _buffer.Add(_by);
                }
                foreach (byte _by in ByteFileVersion)
                {
                    _buffer.Add(_by);
                }

                foreach (byte _by in ByteFileFlags)
                {
                    _buffer.Add(_by);
                }


                return _buffer;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _buffer = new List<byte>();
                foreach (byte _by in LeFileType)
                {
                    _buffer.Add(_by);
                }
                foreach (byte _by in LeFileVersion)
                {
                    _buffer.Add(_by);
                }

                foreach (byte _by in LeFileFlags)
                {
                    _buffer.Add(_by);
                }


                return _buffer;
            }


            public FileHeader()
            {
                FileType = string.Empty;
                FileVersion = string.Empty;
                FileFlags = string.Empty;

                ByteFileType = new byte[4];
                ByteFileVersion = new byte[4];
                ByteFileFlags = new byte[4];

                LeFileType = new byte[4];
                LeFileVersion = new byte[4];
                LeFileFlags = new byte[4];
            }

        }
        public class FileToc
        {
            TocData _tocheader;
            public TocData Tocheader
            {
                get { return _tocheader; }
                set { _tocheader = value; }
            }

            List<TocData> _tocCollection;
            public List<TocData> TocCollection
            {
                get { return _tocCollection; }
                set { _tocCollection = value; }
            }

            public List<byte> GetByteData()
            {
                List<byte> _buffer = new List<byte>();

                foreach (byte _by in Tocheader.ByteName)
                {
                    _buffer.Add(_by);
                }
                foreach (byte _by in Tocheader.ByteOffset)
                {
                    _buffer.Add(_by);
                }

                foreach (TocData toc in TocCollection)
                {
                    foreach (byte _by in toc.ByteName)
                    {
                        _buffer.Add(_by);
                    }
                    foreach (byte _by in toc.ByteOffset)
                    {
                        _buffer.Add(_by);
                    }
                }


                return _buffer;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _buffer = new List<byte>();
                foreach (byte _by in Tocheader.LeName)
                {
                    _buffer.Add(_by);
                }
                foreach (byte _by in Tocheader.LeOffset)
                {
                    _buffer.Add(_by);
                }

                foreach (TocData toc in TocCollection)
                {
                    foreach (byte _by in toc.LeName)
                    {
                        _buffer.Add(_by);
                    }
                    foreach (byte _by in toc.LeOffset)
                    {
                        _buffer.Add(_by);
                    }
                }


                return _buffer;
            }

            public FileToc()
            {
                _tocheader = new TocData();
                _tocCollection = new List<TocData>();
            }

            public class TocData
            {
                string _name;
                public string Name
                {
                    get { return _name; }
                    set { _name = value; }
                }

                long _offset;
                public long Offset
                {
                    get { return _offset; }
                    set { _offset = value; }
                }

                byte[] _byteName;
                public byte[] ByteName
                {
                    get { return _byteName; }
                    set { _byteName = value; }
                }

                byte[] _byteOffset;
                public byte[] ByteOffset
                {
                    get { return _byteOffset; }
                    set { _byteOffset = value; }
                }

                byte[] _leName;
                public byte[] LeName
                {
                    get { return _leName; }
                    set { _leName = value; }
                }

                byte[] _leOffset;
                public byte[] LeOffset
                {
                    get { return _leOffset; }
                    set { _leOffset = value; }
                }

                public void ConvertToByte()
                {

                    ByteName = UEI.QuickSet.Conversion.ConvertToByte(Name, ByteName.Length);
                    ByteOffset = UEI.QuickSet.Conversion.ConvertToByte(Convert.ToInt32(Offset), ByteOffset.Length);



                }

                public void ConvertToLittleEndian()
                {


                    LeName = ByteName;
                    LeOffset = UEI.QuickSet.Conversion.ConvertToLittleEndian(ByteOffset, ByteOffset.Length);


                }

                public int GetEndAddress(int startaddress)
                {
                    int endAddress = startaddress + ByteName.Length + ByteOffset.Length;
                    return endAddress;
                }

                public List<byte> GetByteData()
                {
                    List<byte> _buffer = new List<byte>();

                    foreach (byte _by in ByteName)
                    {
                        _buffer.Add(_by);
                    }
                    foreach (byte _by in ByteOffset)
                    {
                        _buffer.Add(_by);
                    }


                    return _buffer;
                }

                public List<byte> GetConvertedData()
                {
                    List<byte> _buffer = new List<byte>();
                    foreach (byte _by in LeName)
                    {
                        _buffer.Add(_by);
                    }
                    foreach (byte _by in LeOffset)
                    {
                        _buffer.Add(_by);
                    }


                    return _buffer;
                }

                public TocData()
                {
                    Name = string.Empty;
                    Offset = 0;

                    ByteName = new byte[2];
                    ByteOffset = new byte[4];

                    LeName = new byte[2];
                    LeOffset = new byte[4];
                }
            }

        }

        public class BrandRecordHeader
        {
            string _brandHeader;

            public string BrandHeader
            {
                get { return _brandHeader; }
                set { _brandHeader = value; }
            }
            byte[] _byteBrandHeader;

            public byte[] ByteBrandHeader
            {
                get { return _byteBrandHeader; }
                set { _byteBrandHeader = value; }
            }
            byte[] _convertedByteBrandHeader;

            public byte[] ConvertedByteBrandHeader
            {
                get { return _convertedByteBrandHeader; }
                set { _convertedByteBrandHeader = value; }
            }
            int _brandRecordSize;

            public int BrandRecordSize
            {
                get { return _brandRecordSize; }
                set { _brandRecordSize = value; }
            }
            byte[] _byteBrandRecordSize;

            public byte[] ByteBrandRecordSize
            {
                get { return _byteBrandRecordSize; }
                set { _byteBrandRecordSize = value; }
            }
            byte[] _convertedBrandRecordSize;

            public byte[] ConvertedBrandRecordSize
            {
                get { return _convertedBrandRecordSize; }
                set { _convertedBrandRecordSize = value; }
            }
            int _brandsCount;

            public int BrandsCount
            {
                get { return _brandsCount; }
                set { _brandsCount = value; }
            }
            byte[] _byteBrandsCount;

            public byte[] ByteBrandsCount
            {
                get { return _byteBrandsCount; }
                set { _byteBrandsCount = value; }
            }
            byte[] _convertedBrandsCount;

            public byte[] ConvertedBrandsCount
            {
                get { return _convertedBrandsCount; }
                set { _convertedBrandsCount = value; }
            }

            public BrandRecordHeader()
            {
                _brandHeader = string.Empty;
                _brandRecordSize = 0;
                _brandsCount = 0;
                _byteBrandHeader = new byte[2];
                _byteBrandRecordSize = new byte[4];
                _byteBrandsCount = new byte[2];
                _convertedByteBrandHeader = new byte[2];
                _convertedBrandRecordSize = new byte[4];
                _convertedBrandsCount = new byte[2];
            }

            public void ConvertToByte()
            {

                ByteBrandHeader = Conversion.ConvertToByte(BrandHeader, ByteBrandHeader.Length);
                ByteBrandRecordSize = Conversion.ConvertToByte(BrandRecordSize, ByteBrandRecordSize.Length);
                ByteBrandsCount = Conversion.ConvertToByte(BrandsCount, ByteBrandsCount.Length);

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteBrandHeader.Length + ByteBrandRecordSize.Length + ByteBrandsCount.Length;

                return endAddress;
            }

            public void ConvertToEndian()
            {
                //ConvertedByteBrandHeader = Conversion.ConvertToLittleEndian(ByteBrandHeader,ByteBrandHeader.Length);
                ConvertedByteBrandHeader = ByteBrandHeader;
                ConvertedBrandRecordSize = Conversion.ConvertToLittleEndian(ByteBrandRecordSize, ByteBrandRecordSize.Length);
                ConvertedBrandsCount = Conversion.ConvertToLittleEndian(ByteBrandsCount, ByteBrandsCount.Length);



            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteBrandHeader)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandsCount)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedByteBrandHeader)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandsCount)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }
        }
        public class BrandCollections
        {
            List<BrandRecord> _brands;

            public List<BrandRecord> Brands
            {
                get { return _brands; }
                set { _brands = value; }
            }

            public BrandCollections()
            {
                _brands = new List<BrandRecord>();
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (BrandRecord br in _brands)
                {
                    List<byte> b = br.GetByteData();
                    foreach (byte data in b)
                    {
                        _byteData.Add(data);
                    }
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (BrandRecord br in _brands)
                {
                    List<byte> b = br.GetConvertedData();
                    foreach (byte data in b)
                    {
                        _byteData.Add(data);
                    }
                }
                return _byteData;
            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress; //+ ByteType.Length + ByteSizeOfADRRecords.Length;
                int _tempadr = 0;


                List<byte> _byteData = new List<byte>();
                foreach (BrandRecord br in _brands)
                {
                    _tempadr = br.GetEndAddress(0);
                    endAddress = endAddress + _tempadr;

                }

                return endAddress;
            }
        }
        public class BrandRecord
        {

            int _recordSize;

            public int RecordSize
            {
                get { return _recordSize; }
                set { _recordSize = value; }
            }
            byte[] _byteRecordSize;

            public byte[] ByteRecordSize
            {
                get { return _byteRecordSize; }
                set { _byteRecordSize = value; }
            }
            byte[] _convertedRecordSize;

            public byte[] ConvertedRecordSize
            {
                get { return _convertedRecordSize; }
                set { _convertedRecordSize = value; }
            }

            string _brandName;

            public string BrandName
            {
                get { return _brandName; }
                set { _brandName = value; }
            }
            byte[] _byteBrandName;

            public byte[] ByteBrandName
            {
                get { return _byteBrandName; }
                set { _byteBrandName = value; }
            }
            byte[] _convertedBrandName;

            public byte[] ConvertedBrandName
            {
                get { return _convertedBrandName; }
                set { _convertedBrandName = value; }
            }

            public BrandRecord()
            {
                _recordSize = 0;
                _byteRecordSize = new byte[1];
                _convertedRecordSize = new byte[1];

                _brandName = string.Empty;
                //_byteBrandName = new byte[_brandName.Length];
                //_convertedBrandName = new byte[_brandName.Length];
            }

            public void ConvertToByte()
            {


                ByteRecordSize = Conversion.ConvertToByte(RecordSize, ByteRecordSize.Length);
                //ByteBrandName = Conversion.ConvertToByte(BrandName, BrandName.Length);//Need to convert UTF 8 to ASCII first, need to revisit this code
                ByteBrandName = Conversion.ConvertUTF16ToUTF8(BrandName);
                //ByteBrandName = Conversion.ConvertISO3166toByte(BrandName);


            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteRecordSize.Length + BrandName.Length;

                return endAddress;
            }

            public void ConvertToEndian()
            {

                //ConvertedBrandName = Conversion.ConvertToLittleEndian(ByteBrandName,ByteBrandName.Length);
                ConvertedBrandName = ByteBrandName;
                ConvertedRecordSize = Conversion.ConvertToLittleEndian(ByteRecordSize, ByteRecordSize.Length);


            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteBrandName)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ConvertedRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ConvertedBrandName)
                {
                    _byteData.Add(by);
                }
                return _byteData;
            }


        }

        public class ARDRecordHeader : IHeader
        {
            public ARDRecordHeader()
            {
                Type = string.Empty;
                ByteType = new byte[2];

                SizeOfARDRecords = 0;
                ByteSizeOfADRRecords = new byte[4];
            }

            string _type;
            public string Type
            {
                get { return _type; }
                set { _type = value; }
            }
            byte[] _byteType;
            public byte[] ByteType
            {
                get { return _byteType; }
                set { _byteType = value; }
            }
            byte[] _leType;
            public byte[] LeType
            {
                get { return _leType; }
                set { _leType = value; }
            }


            int _sizeOfARDRecords;
            public int SizeOfARDRecords
            {
                get { return _sizeOfARDRecords; }
                set { _sizeOfARDRecords = value; }
            }
            byte[] _byteSizeOfADRRecords;
            public byte[] ByteSizeOfADRRecords
            {
                get { return _byteSizeOfADRRecords; }
                set { _byteSizeOfADRRecords = value; }
            }
            byte[] _leSizeOfARDRecords;
            public byte[] LeSizeOfARDRecords
            {
                get { return _leSizeOfARDRecords; }
                set { _leSizeOfARDRecords = value; }
            }

            List<ARDRecordTOCHeader> _ard;

            public List<ARDRecordTOCHeader> Ard
            {
                get { return _ard; }
                set { _ard = value; }
            }

            public void ConvertToByte()
            {
                ByteType = Conversion.ConvertToByte(Type, ByteType.Length);
                ByteSizeOfADRRecords = Conversion.ConvertToByte(SizeOfARDRecords, ByteSizeOfADRRecords.Length);
                foreach (ARDRecordTOCHeader _ardtoch in Ard)
                {
                    _ardtoch.ConvertToByte();
                }
            }

            public void ConvertToLittleEndian()
            {
                LeType = ByteType;
                LeSizeOfARDRecords = Conversion.ConvertToLittleEndian(ByteSizeOfADRRecords, ByteSizeOfADRRecords.Length);
                foreach (ARDRecordTOCHeader _ardtoch in Ard)
                {
                    _ardtoch.ConvertToLittleEndian();
                }
            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteType.Length + ByteSizeOfADRRecords.Length;
                foreach (ARDRecordTOCHeader _ardtoch in Ard)
                {
                    endAddress += _ardtoch.GetEndAddress(endAddress);
                }
                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteType)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteSizeOfADRRecords)
                {
                    _byteData.Add(by);
                }
                foreach (ARDRecordTOCHeader _ardtoch in Ard)
                {
                    List<byte> _temp = _ardtoch.GetByteData();
                    foreach (byte _te in _temp)
                    {
                        _byteData.Add(_te);
                    }

                }

                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in LeType)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeSizeOfARDRecords)
                {
                    _byteData.Add(by);
                }
                foreach (ARDRecordTOCHeader _ardtoch in Ard)
                {
                    List<byte> _temp = _ardtoch.GetConvertedData();
                    foreach (byte _te in _temp)
                    {
                        _byteData.Add(_te);
                    }

                }

                return _byteData;
            }
        }
        public class ARDRecordTOCHeader : IHeader
        {
            public ARDRecordTOCHeader()
            {
                Type = string.Empty;
                TocRecordSize = 0;
                ArdDataCount = 0;

                ByteType = new byte[2];
                ByteTocRecordSize = new byte[4];
                ByteArdDataCount = new byte[2];

                LeType = new byte[2];
                LeTocRecordSize = new byte[4];
                LeArdDataCount = new byte[2];
            }

            string _type;
            public string Type
            {
                get { return _type; }
                set { _type = value; }
            }
            byte[] _byteType;
            public byte[] ByteType
            {
                get { return _byteType; }
                set { _byteType = value; }
            }
            byte[] _leType;
            public byte[] LeType
            {
                get { return _leType; }
                set { _leType = value; }
            }

            int _tocRecordSize;
            public int TocRecordSize
            {
                get { return _tocRecordSize; }
                set { _tocRecordSize = value; }
            }
            byte[] _byteTocRecordSize;
            public byte[] ByteTocRecordSize
            {
                get { return _byteTocRecordSize; }
                set { _byteTocRecordSize = value; }
            }
            byte[] _leTocRecordSize;
            public byte[] LeTocRecordSize
            {
                get { return _leTocRecordSize; }
                set { _leTocRecordSize = value; }
            }

            int _ardDataCount;
            public int ArdDataCount
            {
                get { return _ardDataCount; }
                set { _ardDataCount = value; }
            }
            byte[] _byteArdDataCount;
            public byte[] ByteArdDataCount
            {
                get { return _byteArdDataCount; }
                set { _byteArdDataCount = value; }
            }
            byte[] _leArdDataCount;
            public byte[] LeArdDataCount
            {
                get { return _leArdDataCount; }
                set { _leArdDataCount = value; }
            }

            List<ARDRecordTOCEntry> _ardRecordTocEntryCollection;

            public List<ARDRecordTOCEntry> ArdRecordTocEntryCollection
            {
                get { return _ardRecordTocEntryCollection; }
                set { _ardRecordTocEntryCollection = value; }
            }

            public void ConvertToByte()
            {
                ByteType = Conversion.ConvertToByte(Type, ByteType.Length);
                ByteTocRecordSize = Conversion.ConvertToByte(TocRecordSize, ByteTocRecordSize.Length);
                ByteArdDataCount = Conversion.ConvertToByte(ArdDataCount, ByteArdDataCount.Length);
                foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                {
                    _ardtocentry.ConvertToByte();
                }
            }

            public void ConvertToLittleEndian()
            {
                LeType = ByteType;
                LeTocRecordSize = Conversion.ConvertToLittleEndian(ByteTocRecordSize, ByteTocRecordSize.Length);
                LeArdDataCount = Conversion.ConvertToLittleEndian(ByteArdDataCount, ByteArdDataCount.Length);
                foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                {
                    _ardtocentry.ConvertToLittleEndian();
                }
            }

            public int GetEndAddress(int startaddress)
            {

                int endAddress = startaddress + ByteType.Length + ByteTocRecordSize.Length + ByteArdDataCount.Length;
                foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                {
                    endAddress += _ardtocentry.GetEndAddress(endAddress);
                }
                return endAddress;
            }

            public int GetEndAddressARDDataEntryRecordCollection(int startaddress)
            {

                int endAddress = startaddress;// +ByteType.Length + ByteTocRecordSize.Length + ByteArdDataCount.Length;
                foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                {
                    endAddress += _ardtocentry.GetEndAddress(endAddress);
                }
                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteType)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteTocRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteArdDataCount)
                {
                    _byteData.Add(by);
                }
                //foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                //{
                //    List<byte> _temp = _ardtocentry.GetByteData();
                //    foreach (byte _by in _temp)
                //    {
                //        _byteData.Add(_by);
                //    }
                //}

                return _byteData;
            }
            public List<byte> GetByteDataARDDataEntryRecordCollection()
            {
                List<byte> _byteData = new List<byte>();
                //foreach (byte by in ByteType)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ByteTocRecordSize)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ByteArdDataCount)
                //{
                //    _byteData.Add(by);
                //}
                foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                {
                    List<byte> _temp = _ardtocentry.GetByteData();
                    foreach (byte _by in _temp)
                    {
                        _byteData.Add(_by);
                    }
                }

                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in LeType)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeTocRecordSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeArdDataCount)
                {
                    _byteData.Add(by);
                }
                //foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                //{
                //    List<byte> _temp = _ardtocentry.GetConvertedData();
                //    foreach (byte _by in _temp)
                //    {
                //        _byteData.Add(_by);
                //    }
                //}
                return _byteData;
            }
            public List<byte> GetConvertedDataARDDataEntryRecordCollection()
            {
                List<byte> _byteData = new List<byte>();
                //foreach (byte by in ByteType)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ByteTocRecordSize)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in ByteArdDataCount)
                //{
                //    _byteData.Add(by);
                //}
                foreach (ARDRecordTOCEntry _ardtocentry in ArdRecordTocEntryCollection)
                {
                    List<byte> _temp = _ardtocentry.GetConvertedData();
                    foreach (byte _by in _temp)
                    {
                        _byteData.Add(_by);
                    }
                }

                return _byteData;
            }
        }
        public class ARDRecordTOCEntry : IHeader
        {
            public ARDRecordTOCEntry()
            {
                Key = 0;
                ByteKey = new byte[4];
                LeKey = new byte[4];

                Offset = 0;
                Byteoffset = new byte[4];
                Leoffset = new Byte[4];
            }

            long _key;
            public long Key
            {
                get { return _key; }
                set { _key = value; }
            }
            byte[] _byteKey;

            public byte[] ByteKey
            {
                get { return _byteKey; }
                set { _byteKey = value; }
            }
            byte[] _leKey;

            public byte[] LeKey
            {
                get { return _leKey; }
                set { _leKey = value; }
            }

            int _offset;
            byte[] _byteoffset;

            public byte[] Byteoffset
            {
                get { return _byteoffset; }
                set { _byteoffset = value; }
            }
            byte[] _leoffset;

            public byte[] Leoffset
            {
                get { return _leoffset; }
                set { _leoffset = value; }
            }

            public int Offset
            {
                get { return _offset; }
                set { _offset = value; }
            }
            List<ARDEntryRecord> _ardentryRecords;

            public List<ARDEntryRecord> ArdentryRecords
            {
                get { return _ardentryRecords; }
                set { _ardentryRecords = value; }
            }

            public void ConvertToByte()
            {
                //ByteKey = Conversion.ConvertToByte(Key, ByteKey.Length);
                //Byteoffset = Conversion.ConvertToByte(Offset, Byteoffset.Length);
                foreach (ARDEntryRecord _ardentryRec in ArdentryRecords)
                {
                    _ardentryRec.ConvertToByte();
                }

            }

            public void ConvertToLittleEndian()
            {

                //LeKey = Conversion.ConvertToLittleEndian(ByteKey, ByteKey.Length);
                //Leoffset = Conversion.ConvertToLittleEndian(Byteoffset, Byteoffset.Length);
                foreach (ARDEntryRecord _ardentryRec in ArdentryRecords)
                {
                    _ardentryRec.ConvertToLittleEndian();
                }
            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress;// +ByteKey.Length + Byteoffset.Length;
                foreach (ARDEntryRecord _ardentryRec in ArdentryRecords)
                {
                    endAddress += _ardentryRec.GetEndAddress(endAddress);
                }

                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                //foreach (byte by in ByteKey)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in Byteoffset)
                //{
                //    _byteData.Add(by);
                //}

                foreach (ARDEntryRecord _ardentryRec in ArdentryRecords)
                {
                    List<byte> _temp = _ardentryRec.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _byteData.Add(by);
                    }
                }

                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                //foreach (byte by in LeKey)
                //{
                //    _byteData.Add(by);
                //}
                //foreach (byte by in Leoffset)
                //{
                //    _byteData.Add(by);
                //}
                foreach (ARDEntryRecord _ardentryRec in ArdentryRecords)
                {
                    List<byte> _temp = _ardentryRec.GetConvertedData();
                    foreach (byte by in _temp)
                    {
                        _byteData.Add(by);
                    }
                }

                return _byteData;
            }


        }
        public class ARDRecordTOCEntryExclusive : IHeader
        {
            public ARDRecordTOCEntryExclusive()
            {
                Key = 0;
                ByteKey = new byte[4];
                LeKey = new byte[4];

                Offset = 0;
                Byteoffset = new byte[4];
                Leoffset = new Byte[4];
            }

            long _key;
            public long Key
            {
                get { return _key; }
                set { _key = value; }
            }
            byte[] _byteKey;

            public byte[] ByteKey
            {
                get { return _byteKey; }
                set { _byteKey = value; }
            }
            byte[] _leKey;

            public byte[] LeKey
            {
                get { return _leKey; }
                set { _leKey = value; }
            }

            int _offset;
            byte[] _byteoffset;

            public byte[] Byteoffset
            {
                get { return _byteoffset; }
                set { _byteoffset = value; }
            }
            byte[] _leoffset;

            public byte[] Leoffset
            {
                get { return _leoffset; }
                set { _leoffset = value; }
            }

            public int Offset
            {
                get { return _offset; }
                set { _offset = value; }
            }

            public void ConvertToByte()
            {

                ByteKey = UEI.QuickSet.Conversion.ConvertToByte((int)Key, ByteKey.Length);
                Byteoffset = Conversion.ConvertToByte(Offset, Byteoffset.Length);


            }

            public void ConvertToLittleEndian()
            {

                LeKey = Conversion.ConvertToLittleEndian(ByteKey, ByteKey.Length);
                Leoffset = Conversion.ConvertToLittleEndian(Byteoffset, Byteoffset.Length);

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteKey.Length + Byteoffset.Length;


                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteKey)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in Byteoffset)
                {
                    _byteData.Add(by);
                }



                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in LeKey)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in Leoffset)
                {
                    _byteData.Add(by);
                }


                return _byteData;
            }


        }
        public class ARDEntryRecord : IHeader
        {
            public class ARDEntryRecordHeader : IHeader
            {

                public ARDEntryRecordHeader()
                {
                    UeiTypeId = string.Empty;
                    EntrySize = 0;
                    BrandRecords = new List<BrandRecords>();

                    ByteueiTypeId = new byte[1];
                    ByteEntrySize = new byte[3];

                    LeueiTypeId = new byte[1];
                    LeEntrySize = new byte[3];
                }
                string _ueiTypeId;

                public string UeiTypeId
                {
                    get { return _ueiTypeId; }
                    set { _ueiTypeId = value; }
                }
                byte[] _byteueiTypeId;

                public byte[] ByteueiTypeId
                {
                    get { return _byteueiTypeId; }
                    set { _byteueiTypeId = value; }
                }
                byte[] _leueiTypeId;

                public byte[] LeueiTypeId
                {
                    get { return _leueiTypeId; }
                    set { _leueiTypeId = value; }
                }

                int _entrySize;

                public int EntrySize
                {
                    get { return _entrySize; }
                    set { _entrySize = value; }
                }
                byte[] _byteEntrySize;

                public byte[] ByteEntrySize
                {
                    get { return _byteEntrySize; }
                    set { _byteEntrySize = value; }
                }
                byte[] _leEntrySize;

                public byte[] LeEntrySize
                {
                    get { return _leEntrySize; }
                    set { _leEntrySize = value; }
                }

                List<BrandRecords> _brandRecords;

                public List<BrandRecords> BrandRecords
                {
                    get { return _brandRecords; }
                    set { _brandRecords = value; }
                }

                //int _brandIdIndex;

                //public int BrandIdIndex
                //{
                //    get { return _brandIdIndex; }
                //    set { _brandIdIndex = value; }
                //}
                //byte[] _byteBrandIndex;

                //public byte[] ByteBrandIndex
                //{
                //    get { return _byteBrandIndex; }
                //    set { _byteBrandIndex = value; }
                //}
                //byte[] _leBrandIndex;

                //public byte[] LeBrandIndex
                //{
                //    get { return _leBrandIndex; }
                //    set { _leBrandIndex = value; }
                //}

                //int _keyRuleCount;

                //public int KeyRuleCount
                //{
                //    get { return _keyRuleCount; }
                //    set { _keyRuleCount = value; }
                //}
                //byte[] _byteKeyRuleCount;

                //public byte[] ByteKeyRuleCount
                //{
                //    get { return _byteKeyRuleCount; }
                //    set { _byteKeyRuleCount = value; }
                //}
                //byte[] _leKeyRuleCount;

                //public byte[] LeKeyRuleCount
                //{
                //    get { return _leKeyRuleCount; }
                //    set { _leKeyRuleCount = value; }
                //}

                //List<FunctionRuleMap> _functionRuleMaps;

                //public List<FunctionRuleMap> FunctionRuleMaps
                //{
                //    get { return _functionRuleMaps; }
                //    set { _functionRuleMaps = value; }
                //}

                public void ConvertToByte()
                {
                    ByteueiTypeId = Conversion.ConvertToByte(UeiTypeId, ByteueiTypeId.Length);
                    ByteEntrySize = Conversion.ConvertToByte(EntrySize, ByteEntrySize.Length);
                    foreach (BrandRecords _br in BrandRecords)
                    {
                        _br.ConvertToByte();
                    }
                    //ByteBrandIndex = Conversion.ConvertToByte(BrandIdIndex, ByteBrandIndex.Length);
                    //ByteKeyRuleCount = Conversion.ConvertToByte(KeyRuleCount, ByteKeyRuleCount.Length);
                }

                public void ConvertToLittleEndian()
                {
                    LeueiTypeId = Conversion.ConvertToLittleEndian(ByteueiTypeId, ByteueiTypeId.Length);
                    LeEntrySize = Conversion.ConvertToLittleEndian(ByteEntrySize, ByteEntrySize.Length);
                    foreach (BrandRecords _br in BrandRecords)
                    {
                        _br.ConvertToLittleEndian();
                    }
                    //LeBrandIndex = Conversion.ConvertToLittleEndian(ByteBrandIndex, ByteBrandIndex.Length);
                    //LeKeyRuleCount = Conversion.ConvertToLittleEndian(ByteKeyRuleCount, ByteKeyRuleCount.Length);
                }

                public int GetEndAddress(int startaddress)
                {
                    int endAddress = startaddress + ByteueiTypeId.Length + ByteEntrySize.Length;
                    foreach (BrandRecords _br in BrandRecords)
                    {
                        endAddress += _br.GetEndAddress(endAddress);
                    }
                    return endAddress;
                }

                public List<byte> GetByteData()
                {
                    List<byte> _byteData = new List<byte>();
                    foreach (byte by in ByteueiTypeId)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteEntrySize)
                    {
                        _byteData.Add(by);
                    }
                    foreach (BrandRecords _br in BrandRecords)
                    {
                        List<byte> _temp = _br.GetByteData();
                        foreach (byte by in _temp)
                        {
                            _byteData.Add(by);
                        }
                    }
                    //foreach (byte by in ByteBrandIndex)
                    //{
                    //    _byteData.Add(by);
                    //}
                    //foreach (byte by in ByteKeyRuleCount)
                    //{
                    //    _byteData.Add(by);
                    //}

                    return _byteData;
                }

                public List<byte> GetConvertedData()
                {
                    List<byte> _byteData = new List<byte>();
                    foreach (byte by in LeueiTypeId)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in LeEntrySize)
                    {
                        _byteData.Add(by);
                    }
                    foreach (BrandRecords _br in BrandRecords)
                    {
                        List<byte> _temp = _br.GetConvertedData();
                        foreach (byte by in _temp)
                        {
                            _byteData.Add(by);
                        }
                    }
                    //foreach (byte by in LeBrandIndex)
                    //{
                    //    _byteData.Add(by);
                    //}
                    //foreach (byte by in LeKeyRuleCount)
                    //{
                    //    _byteData.Add(by);
                    //}

                    return _byteData;
                }
            }

            public class BrandRecords : IHeader
            {
                public BrandRecords()
                {
                    BrandIdIndex = 0;
                    KeyRuleCount = 0;
                    FunctionRuleMaps = new List<FunctionRuleMap>();

                    ByteBrandIndex = new byte[2];
                    ByteIrmacroDelay = new byte[2];
                    ByteKeyRuleCount = new byte[2];

                    LeBrandIndex = new byte[2];
                    LeKeyRuleCount = new byte[2];
                }
                string _name;

                public string Name
                {
                    get { return _name; }
                    set { _name = value; }
                }

                int _brandIdIndex;

                public int BrandIdIndex
                {
                    get { return _brandIdIndex; }
                    set { _brandIdIndex = value; }
                }
                byte[] _byteBrandIndex;

                public byte[] ByteBrandIndex
                {
                    get { return _byteBrandIndex; }
                    set { _byteBrandIndex = value; }
                }
                byte[] _leBrandIndex;

                public byte[] LeBrandIndex
                {
                    get { return _leBrandIndex; }
                    set { _leBrandIndex = value; }
                }

                byte[] _byteIrmacroDelay;

                public byte[] ByteIrmacroDelay
                {
                    get { return _byteIrmacroDelay; }
                    set { _byteIrmacroDelay = value; }
                }
                byte[] _leIrMacroDelay;

                public byte[] LeIrMacroDelay
                {
                    get { return _leIrMacroDelay; }
                    set { _leIrMacroDelay = value; }
                }

                byte[] _bytePowerOnTime;

                public byte[] BytePowerOnTime
                {
                    get { return _bytePowerOnTime; }
                    set { _bytePowerOnTime = value; }
                }

                byte[] _lePowerOnTime;

                public byte[] LePowerOnTime
                {
                    get { return _lePowerOnTime; }
                    set { _lePowerOnTime = value; }
                }

                byte[] _byteReserved;

                public byte[] ByteReserved
                {
                    get { return _byteReserved; }
                    set { _byteReserved = value; }
                }

                byte[] _leReserved;

                public byte[] LeReserved
                {
                    get { return _leReserved; }
                    set { _leReserved = value; }
                }


                int _keyRuleCount;

                public int KeyRuleCount
                {
                    get { return _keyRuleCount; }
                    set { _keyRuleCount = value; }
                }
                byte[] _byteKeyRuleCount;

                public byte[] ByteKeyRuleCount
                {
                    get { return _byteKeyRuleCount; }
                    set { _byteKeyRuleCount = value; }
                }
                byte[] _leKeyRuleCount;

                public byte[] LeKeyRuleCount
                {
                    get { return _leKeyRuleCount; }
                    set { _leKeyRuleCount = value; }
                }

                List<FunctionRuleMap> _functionRuleMaps;

                public List<FunctionRuleMap> FunctionRuleMaps
                {
                    get { return _functionRuleMaps; }
                    set { _functionRuleMaps = value; }
                }



                public void ConvertToByte()
                {
                    ByteBrandIndex = Conversion.ConvertToByte(BrandIdIndex, ByteBrandIndex.Length);
                    ByteIrmacroDelay = Conversion.ConvertToByte(0, 2);
                    BytePowerOnTime = Conversion.ConvertToByte(0, 2);
                    ByteReserved = Conversion.ConvertToByte(0, 2);
                    ByteKeyRuleCount = Conversion.ConvertToByte(KeyRuleCount, ByteKeyRuleCount.Length);
                }

                public void ConvertToLittleEndian()
                {
                    LeBrandIndex = Conversion.ConvertToLittleEndian(ByteBrandIndex, ByteBrandIndex.Length);
                    LeIrMacroDelay = Conversion.ConvertToLittleEndian(ByteIrmacroDelay, ByteIrmacroDelay.Length);
                    LePowerOnTime = Conversion.ConvertToLittleEndian(BytePowerOnTime, BytePowerOnTime.Length);
                    LeReserved = Conversion.ConvertToLittleEndian(ByteReserved, ByteReserved.Length);
                    LeKeyRuleCount = Conversion.ConvertToLittleEndian(ByteKeyRuleCount, ByteKeyRuleCount.Length);
                }

                public int GetEndAddress(int startaddress)
                {
                    int endAddress = startaddress + ByteBrandIndex.Length + LeIrMacroDelay.Length + LePowerOnTime.Length + LeReserved.Length+ByteKeyRuleCount.Length;

                    return endAddress;
                }

                public List<byte> GetByteData()
                {
                    List<byte> _byteData = new List<byte>();

                    foreach (byte by in ByteBrandIndex)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteIrmacroDelay)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in BytePowerOnTime)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteReserved)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteKeyRuleCount)
                    {
                        _byteData.Add(by);
                    }
                    foreach (FunctionRuleMap _frm in FunctionRuleMaps)
                    {
                        List<byte> _temp = _frm.GetByteData();
                        foreach (byte by in _temp)
                        {
                            _byteData.Add(by);
                        }
                    }


                    return _byteData;
                }

                public List<byte> GetConvertedData()
                {
                    List<byte> _byteData = new List<byte>();

                    foreach (byte by in LeBrandIndex)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in LeIrMacroDelay)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in LePowerOnTime)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in LeReserved)
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in LeKeyRuleCount)
                    {
                        _byteData.Add(by);
                    }
                    foreach (FunctionRuleMap _frm in FunctionRuleMaps)
                    {
                        List<byte> _temp = _frm.GetConvertedData();
                        foreach (byte by in _temp)
                        {
                            _byteData.Add(by);
                        }
                    }

                    return _byteData;
                }
            }

            List<ARDEntryRecordHeader> _ardRecordHeader;

            public List<ARDEntryRecordHeader> ArdRecordHeader
            {
                get { return _ardRecordHeader; }
                set { _ardRecordHeader = value; }
            }

            public void ConvertToByte()
            {
                foreach (ARDEntryRecordHeader _ardheader in ArdRecordHeader)
                {
                    _ardheader.ConvertToByte();
                }
            }

            public void ConvertToLittleEndian()
            {
                foreach (ARDEntryRecordHeader _ardheader in ArdRecordHeader)
                {
                    _ardheader.ConvertToLittleEndian();
                }
            }

            public int GetEndAddress(int startaddress)
            {
                int _endAddress = startaddress;
                foreach (ARDEntryRecordHeader _ardheader in ArdRecordHeader)
                {
                    _endAddress += _ardheader.GetEndAddress(_endAddress);
                }
                return _endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (ARDEntryRecordHeader _ardheader in ArdRecordHeader)
                {
                    List<byte> _tempData = _ardheader.GetByteData();
                    foreach (byte _by in _tempData)
                    {
                        _byteData.Add(_by);
                    }
                }
                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (ARDEntryRecordHeader _ardheader in ArdRecordHeader)
                {
                    List<byte> _tempData = _ardheader.GetConvertedData();
                    foreach (byte _by in _tempData)
                    {
                        _byteData.Add(_by);
                    }
                }
                return _byteData;
            }
        }
        public class FunctionRuleMap : IHeader
        {
            public FunctionRuleMap()
            {
                this.UeiFunctionId = string.Empty;
                this.CecDataSize = 0;
                this.CecPriority = 0;
                this.CecCommandCount = 0;
                this.CecCommands = new List<Commands>();

                this.ByteUeiFunctionId = new byte[2];
                this.ByteCecDataSize = new byte[2];
                this.ByteCecPriority = new byte[1];
                this.ByteCecCommandCount = new byte[1];

                this.LeUeiFunctionId = new byte[2];
                this.LeCecDataSize = new byte[2];
                this.LeCecPriority = new byte[1];
                this.LeCecCommandCount = new byte[1];

                this.IrDataSize = 0;
                this.IrPriority = 0;
                this.IrCodesetCount = 0;
                this.IrCodesets = new List<string>();
                this.IrKeyData = new List<string>();
                this.IpDataSize = 0;

                ByteIrDataSize = new byte[1];
                ByteIrPriority = new byte[1];
                ByteIrCodesetCount = new byte[1];
                ByteIrCodesets = new List<byte[]>();
                ByteIrKeyData = new List<byte[]>();
                
                ByteIpDataSize = new byte[2];

                LeIrDataSize = new byte[1];
                LeIrPriority = new byte[1];
                LeIrCodesetCount = new byte[1];
                LeIrCodesets = new List<byte[]>();
                LeIrKeyData = new List<byte[]>();
                LeIpDataSize = new byte[2];
                Bytetimings = new byte[2];
            }
            public byte[] Bytetimings;
            public byte[] Letimings;
            string _ueiFunctionId;
            public string UeiFunctionId
            {
                get { return _ueiFunctionId; }
                set { _ueiFunctionId = value; }
            }
            byte[] _byteUeiFunctionId;
            public byte[] ByteUeiFunctionId
            {
                get { return _byteUeiFunctionId; }
                set { _byteUeiFunctionId = value; }
            }
            byte[] _leUeiFunctionId;
            public byte[] LeUeiFunctionId
            {
                get { return _leUeiFunctionId; }
                set { _leUeiFunctionId = value; }
            }


            int _cecDataSize;
            public int CecDataSize
            {
                get { return _cecDataSize; }
                set { _cecDataSize = value; }
            }
            byte[] _byteCecDataSize;
            public byte[] ByteCecDataSize
            {
                get { return _byteCecDataSize; }
                set { _byteCecDataSize = value; }
            }
            byte[] _leCecDataSize;
            public byte[] LeCecDataSize
            {
                get { return _leCecDataSize; }
                set { _leCecDataSize = value; }
            }

            int _cecPriority;
            public int CecPriority
            {
                get { return _cecPriority; }
                set { _cecPriority = value; }
            }
            byte[] _byteCecPriority;
            public byte[] ByteCecPriority
            {
                get { return _byteCecPriority; }
                set { _byteCecPriority = value; }
            }
            byte[] _leCecPriority;
            public byte[] LeCecPriority
            {
                get { return _leCecPriority; }
                set { _leCecPriority = value; }
            }

            int _cecCommandCount;
            public int CecCommandCount
            {
                get { return _cecCommandCount; }
                set { _cecCommandCount = value; }
            }
            byte[] _byteCecCommandCount;
            public byte[] ByteCecCommandCount
            {
                get { return _byteCecCommandCount; }
                set { _byteCecCommandCount = value; }
            }
            byte[] _leCecCommandCount;
            public byte[] LeCecCommandCount
            {
                get { return _leCecCommandCount; }
                set { _leCecCommandCount = value; }
            }



            int _irDataSize;
            byte[] _byteIrDataSize;
            public byte[] ByteIrDataSize
            {
                get { return _byteIrDataSize; }
                set { _byteIrDataSize = value; }
            }
            byte[] _leIrDataSize;
            public byte[] LeIrDataSize
            {
                get { return _leIrDataSize; }
                set { _leIrDataSize = value; }
            }
            public int IrDataSize
            {
                get { return _irDataSize; }
                set { _irDataSize = value; }
            }

            int _irPriority;
            byte[] _byteIrPriority;
            public byte[] ByteIrPriority
            {
                get { return _byteIrPriority; }
                set { _byteIrPriority = value; }
            }
            byte[] _leIrPriority;
            public byte[] LeIrPriority
            {
                get { return _leIrPriority; }
                set { _leIrPriority = value; }
            }
            public int IrPriority
            {
                get { return _irPriority; }
                set { _irPriority = value; }
            }

            int _irCodesetCount;
            byte[] _byteIrCodesetCount;
            public byte[] ByteIrCodesetCount
            {
                get { return _byteIrCodesetCount; }
                set { _byteIrCodesetCount = value; }
            }
            byte[] _leIrCodesetCount;
            public byte[] LeIrCodesetCount
            {
                get { return _leIrCodesetCount; }
                set { _leIrCodesetCount = value; }
            }
            public int IrCodesetCount
            {
                get { return _irCodesetCount; }
                set { _irCodesetCount = value; }
            }

            List<string> _irCodesets;
            List<byte[]> _byteIrCodesets;
            private List<byte[]> ByteIrCodesets
            {
                get { return _byteIrCodesets; }
                set { _byteIrCodesets = value; }
            }
            List<byte[]> _leIrCodesets;
            private List<byte[]> LeIrCodesets
            {
                get { return _leIrCodesets; }
                set { _leIrCodesets = value; }
            }
            public List<string> IrCodesets
            {
                get { return _irCodesets; }
                set { _irCodesets = value; }
            }

            List<string> _irKeyData;
            public List<string> IrKeyData
            {
                get { return _irKeyData; }
                set { _irKeyData = value; }
            }
            List<byte[]> _byteIrKeyData;
            private List<byte[]> ByteIrKeyData
            {
                get { return _byteIrKeyData; }
                set { _byteIrKeyData = value; }
            }
            List<byte[]> _leIrKeyData;
            private List<byte[]> LeIrKeyData
            {
                get { return _leIrKeyData; }
                set { _leIrKeyData = value; }
            }

            int _ipDataSize;
            byte[] _byteIpDataSize;
            public byte[] ByteIpDataSize
            {
                get { return _byteIpDataSize; }
                set { _byteIpDataSize = value; }
            }
            byte[] _leIpDataSize;
            public byte[] LeIpDataSize
            {
                get { return _leIpDataSize; }
                set { _leIpDataSize = value; }
            }
            public int IpDataSize
            {
                get { return _ipDataSize; }
                set { _ipDataSize = value; }
            }

            List<Commands> _cecCommands;
            public List<Commands> CecCommands
            {
                get { return _cecCommands; }
                set { _cecCommands = value; }
            }

            Commands _cmds = new Commands();

            //public class CECCommand : IHeader
            //{
            //    public CECCommand()
            //    {
            //        this.IrDataSize = 0;
            //        this.IrPriority = 0;
            //        this.IrCodesetCount = 0;
            //        this.IrCodesets = new List<string>();
            //        this.IpDataSize = 0;

            //        ByteIrDataSize = new byte[1];
            //        ByteIrPriority = new byte[1];
            //        ByteIrCodesetCount = new byte[1];
            //        ByteIrCodesets = new List<byte[]>();
            //        ByteIpDataSize = new byte[2];

            //        LeIrDataSize = new byte[1];
            //        LeIrPriority = new byte[1];
            //        LeIrCodesetCount = new byte[1];
            //        LeIrCodesets = new List<byte[]>();
            //        LeIpDataSize = new byte[2];

            //    }
            //    int _irDataSize;
            //    byte[] _byteIrDataSize;
            //    public byte[] ByteIrDataSize
            //    {
            //        get { return _byteIrDataSize; }
            //        set { _byteIrDataSize = value; }
            //    }
            //    byte[] _leIrDataSize;
            //    public byte[] LeIrDataSize
            //    {
            //        get { return _leIrDataSize; }
            //        set { _leIrDataSize = value; }
            //    }
            //    public int IrDataSize
            //    {
            //        get { return _irDataSize; }
            //        set { _irDataSize = value; }
            //    }

            //    int _irPriority;
            //    byte[] _byteIrPriority;
            //    public byte[] ByteIrPriority
            //    {
            //        get { return _byteIrPriority; }
            //        set { _byteIrPriority = value; }
            //    }
            //    byte[] _leIrPriority;
            //    public byte[] LeIrPriority
            //    {
            //        get { return _leIrPriority; }
            //        set { _leIrPriority = value; }
            //    }
            //    public int IrPriority
            //    {
            //        get { return _irPriority; }
            //        set { _irPriority = value; }
            //    }

            //    int _irCodesetCount;
            //    byte[] _byteIrCodesetCount;
            //    public byte[] ByteIrCodesetCount
            //    {
            //        get { return _byteIrCodesetCount; }
            //        set { _byteIrCodesetCount = value; }
            //    }
            //    byte[] _leIrCodesetCount;
            //    public byte[] LeIrCodesetCount
            //    {
            //        get { return _leIrCodesetCount; }
            //        set { _leIrCodesetCount = value; }
            //    }
            //    public int IrCodesetCount
            //    {
            //        get { return _irCodesetCount; }
            //        set { _irCodesetCount = value; }
            //    }

            //    List<string> _irCodesets;
            //    List<byte[]> _byteIrCodesets;
            //    private List<byte[]> ByteIrCodesets
            //    {
            //        get { return _byteIrCodesets; }
            //        set { _byteIrCodesets = value; }
            //    }
            //    List<byte[]> _leIrCodesets;
            //    private List<byte[]> LeIrCodesets
            //    {
            //        get { return _leIrCodesets; }
            //        set { _leIrCodesets = value; }
            //    }
            //    public List<string> IrCodesets
            //    {
            //        get { return _irCodesets; }
            //        set { _irCodesets = value; }
            //    }

            //    int _ipDataSize;
            //    byte[] _byteIpDataSize;
            //    public byte[] ByteIpDataSize
            //    {
            //        get { return _byteIpDataSize; }
            //        set { _byteIpDataSize = value; }
            //    }
            //    byte[] _leIpDataSize;
            //    public byte[] LeIpDataSize
            //    {
            //        get { return _leIpDataSize; }
            //        set { _leIpDataSize = value; }
            //    }
            //    public int IpDataSize
            //    {
            //        get { return _ipDataSize; }
            //        set { _ipDataSize = value; }
            //    }


            //    public void ConvertToByte()
            //    {
            //        throw new NotImplementedException();
            //    }

            //    public void ConvertToLittleEndian()
            //    {
            //        throw new NotImplementedException();
            //    }

            //    public int GetEndAddress(int startaddress)
            //    {
            //        throw new NotImplementedException();
            //    }

            //    public List<byte> GetByteData()
            //    {
            //        throw new NotImplementedException();
            //    }

            //    public List<byte> GetConvertedData()
            //    {
            //        throw new NotImplementedException();
            //    }
            //}

            public void ConvertToByte()
            {
                int _temp = Convert.ToInt32(UeiFunctionId.Replace("0x", "").Trim(), 16);
                ByteUeiFunctionId = Conversion.ConvertToByte(_temp, ByteUeiFunctionId.Length);
                ByteCecDataSize = Conversion.ConvertToByte(CecDataSize, ByteCecDataSize.Length);
                ByteCecPriority = Conversion.ConvertToByte(CecPriority, ByteCecPriority.Length);
                ByteCecCommandCount = Conversion.ConvertToByte(CecCommandCount, ByteCecCommandCount.Length);
                foreach (Commands cmd in CecCommands)
                {
                    cmd.ConvertToByte();
                }

                ByteIrDataSize = Conversion.ConvertToByte(IrDataSize, ByteIrDataSize.Length);
                ByteIrPriority = Conversion.ConvertToByte(IrPriority, ByteIrPriority.Length);
                ByteIrCodesetCount = Conversion.ConvertToByte(IrCodesetCount, ByteIrCodesetCount.Length);
                byte[] _byteCs = new byte[3];
                byte[] _byteKd = new byte[1];
                foreach (string cs in IrCodesets)
                {
                    _byteCs = new byte[3];
                    string _mode = cs.Substring(0, 1);
                    byte[] _byteMode = Conversion.ConvertToByte(_mode, _mode.Length);
                    string _setupcode = cs.Substring(1, 4);
                    byte[] _byteSetupCode = Conversion.ConvertToByte(int.Parse(_setupcode), 2);
                    byte[] _convertedByte = new byte[3];
                    _convertedByte[0] = _byteMode[0];
                    _convertedByte[1] = _byteSetupCode[0];
                    _convertedByte[2] = _byteSetupCode[1];
                    //_byteCs = Conversion.ConvertToByte(cs, _byteCs.Length);//T2051 T uses 1 byte and 2051 uses other 2 bytes
                    ByteIrCodesets.Add(_convertedByte);

                }

                foreach (string kd in IrKeyData)
                {
                    _byteKd = new byte[1];
                    int decValue = Convert.ToInt32(kd, 16);
                    ByteIrKeyData.Add(Conversion.ConvertToByte(decValue, 1));
                }
                Bytetimings = Conversion.ConvertToByte(0, 2);
                ByteIpDataSize = Conversion.ConvertToByte(IpDataSize, ByteIpDataSize.Length);


            }

            public void ConvertToLittleEndian()
            {
                LeUeiFunctionId = Conversion.ConvertToLittleEndian(ByteUeiFunctionId, ByteUeiFunctionId.Length);
                LeCecDataSize = Conversion.ConvertToLittleEndian(ByteCecDataSize, ByteCecDataSize.Length);
                LeCecPriority = Conversion.ConvertToLittleEndian(ByteCecPriority, ByteCecPriority.Length);
                LeCecCommandCount = Conversion.ConvertToLittleEndian(ByteCecCommandCount, ByteCecCommandCount.Length);
                foreach (Commands cmd in CecCommands)
                {
                    cmd.ConvertToLittleEndian();
                }
                LeIrDataSize = Conversion.ConvertToLittleEndian(ByteIrDataSize, ByteIrDataSize.Length);
                LeIrPriority = Conversion.ConvertToLittleEndian(ByteIrPriority, ByteIrPriority.Length);
                LeIrCodesetCount = Conversion.ConvertToLittleEndian(ByteIrCodesetCount, ByteIrCodesetCount.Length);
                //byte[] _byteCs = new byte[3];

                byte[] _byteCs = new byte[3];
                //byte[] _bytekd = new byte[1];
                foreach (byte[] cs in ByteIrCodesets)
                {
                    _byteCs = new byte[3];
                    _byteCs = Conversion.ConvertToLittleEndian(cs, 3);
                    LeIrCodesets.Add(_byteCs);

                }
                foreach (byte[] by in ByteIrKeyData)
                {

                    LeIrKeyData.Add(by);

                }
                Letimings = Conversion.ConvertToLittleEndian(Bytetimings, 2);
                LeIpDataSize = Conversion.ConvertToLittleEndian(ByteIpDataSize, ByteIpDataSize.Length);
            }

            public int GetEndAddress(int startaddress)
            {
                int endaddress = startaddress + ByteUeiFunctionId.Length + ByteCecDataSize.Length + ByteCecPriority.Length + ByteCecCommandCount.Length;
                foreach (Commands cm in CecCommands)
                {
                    int _staddrs = 0;
                    endaddress += cm.GetEndAddress(_staddrs);
                }
                endaddress += ByteIrDataSize.Length + ByteIrPriority.Length + ByteIrCodesetCount.Length;

                foreach (byte[] by in ByteIrCodesets)
                {
                    endaddress += by.Length;
                }

                foreach (byte[] by in ByteIrKeyData)
                {
                    endaddress += by.Length;
                }
                endaddress += 2;//Bytetiming length
                endaddress += ByteIpDataSize.Length;
                return endaddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteUeiFunctionId)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteCecDataSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteCecPriority)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteCecCommandCount)
                {
                    _byteData.Add(by);
                }
                foreach (Commands _cmds in CecCommands)
                {
                    List<byte> _temp = _cmds.GetByteData();
                    foreach (byte by in _temp)
                    {
                        _byteData.Add(by);
                    }
                }

                foreach (byte by in ByteIrDataSize)
                {
                    _byteData.Add(by);
                }



                foreach (byte by in ByteIrPriority)
                {
                    _byteData.Add(by);
                }

                foreach (byte by in ByteIrCodesetCount)
                {
                    _byteData.Add(by);
                }

                //foreach (byte[] _id in ByteIrCodesets)
                //{
                //    foreach (byte by in _id)
                //    {
                //        _byteData.Add(by);
                //    }
                //}

                for (int i = 0; i < ByteIrCodesets.Count; i++)
                {
                    foreach (byte by in ByteIrCodesets[i])
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in ByteIrKeyData[i])
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in Bytetimings)
                    {
                        _byteData.Add(by);
                    }
                }

                foreach (byte by in ByteIpDataSize)
                {
                    _byteData.Add(by);
                }



                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in LeUeiFunctionId)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeCecDataSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeCecPriority)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeCecCommandCount)
                {
                    _byteData.Add(by);
                }
                foreach (Commands _cmds in CecCommands)
                {
                    List<byte> _temp = _cmds.GetConvertedData();
                    foreach (byte by in _temp)
                    {
                        _byteData.Add(by);
                    }
                }

                foreach (byte by in LeIrDataSize)
                {
                    _byteData.Add(by);
                }

                foreach (byte by in LeIrPriority)
                {
                    _byteData.Add(by);
                }

                foreach (byte by in LeIrCodesetCount)
                {
                    _byteData.Add(by);
                }

                //foreach (byte[] _id in LeIrCodesets)
                //{
                //    foreach (byte by in _id)
                //    {
                //        _byteData.Add(by);
                //    }
                //}

                for (int i = 0; i < LeIrCodesets.Count; i++)
                {
                    foreach (byte by in LeIrCodesets[i])
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in LeIrKeyData[i])
                    {
                        _byteData.Add(by);
                    }
                    foreach (byte by in Letimings)
                    {
                        _byteData.Add(by);
                    }
                }

                foreach (byte by in LeIpDataSize)
                {
                    _byteData.Add(by);
                }



                return _byteData;
            }
        }
        public class Commands : IHeader
        {
            public Commands()
            {
                Size = 0;
                Cmdvalue = new List<int>();

                ByteSize = new byte[1];
                LeSize = new byte[1];



            }
            int _size;
            public int Size
            {
                get { return _size; }
                set { _size = value; }
            }

            byte[] _byteSize;

            public byte[] ByteSize
            {
                get { return _byteSize; }
                set { _byteSize = value; }
            }
            byte[] _leSize;

            public byte[] LeSize
            {
                get { return _leSize; }
                set { _leSize = value; }
            }

            List<int> _cmdvalue;
            public List<int> Cmdvalue
            {
                get { return _cmdvalue; }
                set { _cmdvalue = value; }
            }

            byte[] _byteCmdValue;
            public byte[] ByteCmdValue
            {
                get { return _byteCmdValue; }
                set { _byteCmdValue = value; }
            }

            byte[] _leCmdValue;
            public byte[] LeCmdValue
            {
                get { return _leCmdValue; }
                set { _leCmdValue = value; }
            }

            string _keyname;
            public string Keyname
            {
                get { return _keyname; }
                set { _keyname = value; }
            }


            public void ConvertToByte()
            {
                ByteSize = Conversion.ConvertToByte(Size, ByteSize.Length);
                ByteCmdValue = new byte[Cmdvalue.Count];
                List<byte> _temp = new List<byte>();
                ByteCmdValue = new byte[Cmdvalue.Count];
                int counter = 0;
                foreach (int cm in Cmdvalue)
                {
                    //_temp = Conversion.ConvertToByte(cm, 1);
                    ByteCmdValue[counter] = (byte)cm;
                    counter++;
                }


            }

            public void ConvertToLittleEndian()
            {
                LeSize = Conversion.ConvertToLittleEndian(ByteSize, ByteSize.Length);
                LeCmdValue = new byte[Cmdvalue.Count];
                List<byte> _temp = new List<byte>();

                //int counter = 0;
                for(int i=0;i<Cmdvalue.Count;i++)
                {
                    LeCmdValue[i] = ByteCmdValue[i];
                }
            }

            public int GetEndAddress(int startaddress)
            {
                int endadress = startaddress + ByteSize.Length + ByteCmdValue.Length;
                return endadress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteCmdValue)
                {
                    _byteData.Add(by);
                }


                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in LeSize)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeCmdValue)
                {
                    _byteData.Add(by);
                }


                return _byteData;
            }
        }

        public class FPAddress
        {
            public long fingerprint;
            public int startaddress = 0;
            public int endaddress = 0;
            public List<dev> devices = new List<dev>();
            public class dev
            {
                public string devname;
                public int startaddress;
                public int endaddress;
            }
        }

        public class FileEndRecord : IHeader
        {
            public FileEndRecord()
            {
                Type = string.Empty;
                ByteType = new byte[2];

                Checksum = 0;
                ByteChecksum = new byte[2];
            }

            string _type;
            public string Type
            {
                get { return _type; }
                set { _type = value; }
            }
            byte[] _byteType;
            public byte[] ByteType
            {
                get { return _byteType; }
                set { _byteType = value; }
            }
            byte[] _leType;
            public byte[] LeType
            {
                get { return _leType; }
                set { _leType = value; }
            }


            Int64 _checksum;
            public Int64 Checksum
            {
                get { return _checksum; }
                set { _checksum = value; }
            }
            byte[] _byteChecksum;
            public byte[] ByteChecksum
            {
                get { return _byteChecksum; }
                set { _byteChecksum = value; }
            }
            byte[] _leChecksum;
            public byte[] LeChecksum
            {
                get { return _leChecksum; }
                set { _leChecksum = value; }
            }


            public void ConvertToByte()
            {
                ByteType = Conversion.ConvertToByte(Type, ByteType.Length);
                ByteChecksum = Conversion.ConvertToByte((int)Checksum, ByteChecksum.Length);

            }

            public void ConvertToLittleEndian()
            {
                LeType = ByteType;
                LeChecksum = Conversion.ConvertToLittleEndian(ByteChecksum, ByteChecksum.Length);

            }

            public int GetEndAddress(int startaddress)
            {
                int endAddress = startaddress + ByteType.Length + ByteChecksum.Length;

                return endAddress;
            }

            public List<byte> GetByteData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in ByteType)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in ByteChecksum)
                {
                    _byteData.Add(by);
                }


                return _byteData;
            }

            public List<byte> GetConvertedData()
            {
                List<byte> _byteData = new List<byte>();
                foreach (byte by in LeType)
                {
                    _byteData.Add(by);
                }
                foreach (byte by in LeChecksum)
                {
                    _byteData.Add(by);
                }


                return _byteData;
            }
        }

        #region Function Rule Map Data
        public class FunctionRuleMapData
        {
            public FunctionRuleMapData()
            {
                _figerPrintRecords = new List<FingerPrintRecord>();
            }
            List<FingerPrintRecord> _figerPrintRecords;
            public List<FingerPrintRecord> FigerPrintRecords
            {
                get { return _figerPrintRecords; }
                set { _figerPrintRecords = value; }
            }
        }
        public class FingerPrintRecord
        {
            public FingerPrintRecord()
            {
                FingerprintRecord = string.Empty;
                Devices = new List<Device>();

            }

            string _fingerprintRecord;
            public string FingerprintRecord
            {
                get { return _fingerprintRecord; }
                set { _fingerprintRecord = value; }
            }
            List<Device> _devices;

            public List<Device> Devices
            {
                get { return _devices; }
                set { _devices = value; }
            }


        }
        public class Brand
        {
            public Brand()
            {
                Name = string.Empty;
                Keys = new List<FP_Key>();

            }
            string _name;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            List<FP_Key> _keys;

            public List<FP_Key> Keys
            {
                get { return _keys; }
                set { _keys = value; }
            }

        }
        public class Device
        {
            public Device()
            {
                Name = string.Empty;
                Brands = new List<Brand>();
            }
            string _name;

            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
            List<Brand> _brands;

            public List<Brand> Brands
            {
                get { return _brands; }
                set { _brands = value; }
            }
        }
        public class FP_Key
        {
            public FP_Key()
            {
                Id = string.Empty;
                Name = string.Empty;
            }
            string _id;
            public string Id
            {
                get { return _id; }
                set { _id = value; }
            }
            string _name;
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }
            CEC _cecinfo;

            public CEC Cecinfo
            {
                get { return _cecinfo; }
                set { _cecinfo = value; }
            }
            IR _irinfo;

            public IR Irinfo
            {
                get { return _irinfo; }
                set { _irinfo = value; }
            }
            IP _ipinfo;

            public IP Ipinfo
            {
                get { return _ipinfo; }
                set { _ipinfo = value; }
            }
        }
        public class CEC : IData
        {
            int _dataSize;
            string _priority;
            int _commandcount;
            List<Commands> _commands;

            public int DataSize
            {
                get
                {
                    return _dataSize;
                }
                set
                {
                    _dataSize = value;
                }
            }
            public string Priority
            {
                get
                {
                    return _priority;
                }
                set
                {
                    _priority = value;
                }
            }
            public int Commandcount
            {
                get
                {
                    return _commandcount;
                }
                set
                {
                    _commandcount = value;
                }
            }
            public List<Commands> Commands
            {
                get
                {
                    return _commands;
                }
                set
                {
                    _commands = value;
                }
            }
        }
        public class IR : IData
        {
            int _dataSize;
            string _priority;
            int _commandcount;
            List<Commands> _commands;
            int _irCodesetcount;
            int _irkeydata;

            public IR()
            {
                DataSize = 0;
                Priority = string.Empty;
                Commandcount = 0;
                Commands = new List<Commands>();
                IrCodesetcount = 0;
                Irkeydata = 0;
                IrCodesetKeyId = new Dictionary<string, string>();
            }

            public int Irkeydata
            {
                get { return _irkeydata; }
                set { _irkeydata = value; }
            }

            public int IrCodesetcount
            {
                get { return _irCodesetcount; }
                set { _irCodesetcount = value; }
            }
            string _ircodesets;

            public string Ircodesets
            {
                get { return _ircodesets; }
                set { _ircodesets = value; }
            }
            private Dictionary<string, string> _irCodesetKeyId;

            public Dictionary<string, string> IrCodesetKeyId
            {
                get { return _irCodesetKeyId; }
                set { _irCodesetKeyId = value; }
            }


            string _keydata;

            public string Keydata
            {
                get { return _keydata; }
                set { _keydata = value; }
            }

            public int DataSize
            {
                get
                {
                    return _dataSize;
                }
                set
                {
                    _dataSize = value;
                }
            }
            public string Priority
            {
                get
                {
                    return _priority;
                }
                set
                {
                    _priority = value;
                }
            }
            public int Commandcount
            {
                get
                {
                    return _commandcount;
                }
                set
                {
                    _commandcount = value;
                }
            }
            public List<Commands> Commands
            {
                get
                {
                    return _commands;
                }
                set
                {
                    _commands = value;
                }
            }
        }
        public class IP : IData
        {
            int _dataSize;
            string _priority;
            int _commandcount;
            List<Commands> _commands;
            string _ipcommand;

            public string Ipcommand
            {
                get { return _ipcommand; }
                set { _ipcommand = value; }
            }

            public int DataSize
            {
                get
                {
                    return _dataSize;
                }
                set
                {
                    _dataSize = value;
                }
            }
            public string Priority
            {
                get
                {
                    return _priority;
                }
                set
                {
                    _priority = value;
                }
            }
            public int Commandcount
            {
                get
                {
                    return _commandcount;
                }
                set
                {
                    _commandcount = value;
                }
            }
            public List<Commands> Commands
            {
                get
                {
                    return _commands;
                }
                set
                {
                    _commands = value;
                }
            }
        }

        #endregion
    
}
