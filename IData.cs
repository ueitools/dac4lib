﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UEI.QuickSet.DAC4Lib
{
    public interface IData
    {
        int DataSize { get; set; }
        string Priority { get; set; }
        int Commandcount { get; set; }
        List<Commands> Commands { get; set; }

    }
}
