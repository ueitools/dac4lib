﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace UEI.QuickSet.DAC4Lib
{
    public interface IHeader
    {
        void ConvertToByte();
        void ConvertToLittleEndian();
        int GetEndAddress(int startaddress);
        List<byte> GetByteData();
        List<byte> GetConvertedData();
    }
}
